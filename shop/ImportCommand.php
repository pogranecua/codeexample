<?php
/**
 * Created by PhpStorm.
 * User: peter
 * E-mail: petro.stasuk.990@gmail.com
 * Date: 23.05.14
 * Time: 10:14
 */

Yii::import('application.modules.shop.models.*');

class ImportCommand extends CConsoleCommand
{
    protected $processGoods = array();

    public function run($argc)
    {
        $file = ImportFile::model()->find('status='.ImportFile::STATUS_NEW);
        if ($file === null)
            return;
        ImportFile::model()->updateByPk($file->id, array('status'=>ImportFile::STATUS_IN_PROGRESS));

        $dir = dirname(__FILE__).'/../../xml/';

        $xml = simplexml_load_file($dir.$file->file);

        foreach ($xml->Items as $item) {
            $goodId = $this->getGoodId($item);
            if ($goodId === false)
                continue;
            $goodItem = new ShopGoodItem();
            $goodItem->good_id = $goodId;
            $goodItem->price = isset($item->price) ? $item->price : null;
            $goodItem->count_in_box = isset($item->count) ? $item->count : null;
            $goodItem->size = isset($item->size) ? $item->size : null;
            $goodItem->rostovka = isset($item->sizerange) ? $item->sizerange : null;
            $goodItem->color = isset($item->color) ? $item->color : null;
            $goodItem->save();
        }
        ImportFile::model()->updateByPk($file->id, array('status'=>ImportFile::STATUS_COMPLETE));
        $deleteGoods = Good::model()->findAllByAttributes(array(
            'status'=>Good::STATUS_DELETED
        ));
        foreach ($deleteGoods as $good)
            $good->delete();
    }

    public function getGoodId($itemXml)
    {
        if (!isset($itemXml->article))
            return false;
        $good = Good::model()->findByAttributes(array(
            'article'=>$itemXml->article
        ));
        if ($good === null) {
            $good = new Good();
            $good->article = $itemXml->article;
        }
        $good->name = isset($itemXml->name) ? $itemXml->name : null;
        $good->articlegroup = isset($itemXml->articlegroup) ? $itemXml->articlegroup : null;
        $good->is_novelty = isset($itemXml->novelty) ? $itemXml->novelty : null;
        $good->price = isset($itemXml->price) ? $itemXml->price : null;
        $good->availability = isset($itemXml->availability) ? $itemXml->availability : null;
        $good->category_id = isset($itemXml->category) ? $this->getCategoryId($itemXml->category) : null;
        $good->brand_id = $this->getBrandId($itemXml);
        $good->description = isset($itemXml->description) ? $itemXml->description : '';
        $good->status = isset($itemXml->status) ? $itemXml->status : Good::STATUS_ACTIVE;
        if ($good->save()) {
            $images = GoodImage::model()->findAllByAttributes(array(
                'good_id'=>$good->id
            ));

            foreach ($images as $image)
                $image->delete();

            if (isset($itemXml->images) && isset($itemXml->images->image)) {
                foreach ($itemXml->images->image as $image) {
                    $is_main = 0;
                    $fileExtension = 'jpeg';
                    if (strpos($image, '.') !== false) {
                        $imageItems = explode('.', $image);
                        if (count($imageItems) === 2)
                            $fileExtension = $imageItems[1];
                    }
                    if (isset($itemXml->mainimage) && strtolower($itemXml->mainimage.'.'.$fileExtension) == strtolower($image))
                        $is_main = 1;
                    Yii::app()->db->createCommand()
                        ->insert('{{shop_good_image}}', array(
                            'good_id'=>$good->id,
                            'image'=>$image,
                            'is_main'=>$is_main,
                            'status'=>1
                        ));
                }
            }

            if (isset($itemXml->attributes) && isset($itemXml->attributes->attr)) {
                foreach ($itemXml->attributes->attr as $attr) {
                    $good->addParseAttribute($attr['name'], $attr['code'], $attr);
                }
            }

        } else
            return false;

        if ($good->isNewRecord)
            return false;
        else {
            if (!in_array($good->id, $this->processGoods)) {
                $this->processGoods[] = $good->id;
                ShopGoodItem::model()->deleteAllByAttributes(array(
                    'good_id'=>$good->id
                ));
            }
            return $good->id;
        }

    }

    public function getCategoryId($name)
    {
        $model = Category::model()->findByAttributes(array(
            'name'=>$name
        ));
        if ($model === null) {
            $model = new Category;
            $model->name = $name;
            $model->saveNode();
            $model->refresh();
        }
        return $model->id;
    }

    public function getBrandId($itemXml)
    {
        if (!isset($itemXml->brand))
            return null;
        $model = Brand::model()->findByAttributes(array(
            'name'=>$itemXml->brand
        ));
        if ($model === null) {
            $model = new Brand;
            $model->name = $itemXml->brand;
            $model->status = 1;
            $model->save();
            $model->refresh();
        }
        return $model->id;
    }
} 