<?php
/**
 * Created by PhpStorm.
 * User: peter
 * E-mail: petro.stasuk.990@gmail.com
 * Date: 23.05.14
 * Time: 10:14
 */

Yii::import('application.modules.shop.models.*');

class ImportCommand_ver_2 extends CConsoleCommand
{
    protected $processGoods = array();

    public function run($argc)
    {
        try {
            $file = ImportFile::model()->find('status='.ImportFile::STATUS_NEW);//'status='.ImportFile::STATUS_NEW
            if ($file === null)
                return;
            ImportFile::model()->updateByPk($file->id, array('status'=>ImportFile::STATUS_IN_PROGRESS));
            Good::model()->updateAll(array('status'=>Good::STATUS_ONE_C));
            $dir = dirname(__FILE__).'/../../xml/';
            $xml = simplexml_load_file($dir.$file->file);
            foreach ($xml->Items as $item) {
                $kod = isset($item->kod) ? $item->kod : null;
                if ($kod == 'Краска') {
                    if (isset($item->attributes->attr)) {
                        foreach ($item->attributes->attr as $attr)
                            if ($attr['name1'] == 'ЦветКраски') {
                                $kod = $attr['code2'];
                                break;
                            }
                    }
                }
                if ($kod === null)
                    continue;
                $good = Good::model()->findByAttributes(array('kod'=>$kod));
                if ($good === null) {
                    $good = new Good();
                    $good->kod = $kod;
                }
                $good->article = isset($item->article)?$item->article:'';
                $good->name = isset($item->name) ? $item->name : null;
                $good->price = isset($item->price) ? $item->price : null;
                $good->availability_count = isset($item->count) ? $item->count : null;
                $good->availability = isset($item->availability) ? $item->availability : null;
                $good->brand_id = $this->getBrandId($item);
                $good->category_id = isset($item->categories) ? $this->getCategoryId($item->categories) : null;
                $good->description = isset($item->Opys) ? $item->Opys : '';
                $good->how_to_use = isset($item->SposobRriminenija) ? $item->SposobRriminenija : '';
                $good->composition = isset($item->Sostav) ? $item->Sostav : '';
                $good->status = Good::STATUS_ACTIVE;
                if ($good->save()) {
                    //delete all good old images and add new
                    $this->addGoodImages($item,$good);
                    //delete all good images
                    GoodColor::model()->deleteAllByAttributes(array('good_id'=>$good->id));
                    // delete all good characteristic
                    GoodAttrVal::model()->deleteAllByAttributes(array('good_id'=>$good->id));
                    //add good characteristic
                    if (isset($item->attributes) && isset($item->attributes->attr)) {
                        foreach ($item->attributes->attr as $attr) {
                            $good->addParseAttribute($attr['name1'], $attr['name2'], $attr['code2']);
                        }
                    }

                } else{

                return false;
                }
            }
            $oldGoods = Good::model()->findAllByAttributes(array('status'=>Good::STATUS_ONE_C));
            foreach($oldGoods as $oldGood)
            {
                $oldGood->delete();
            }
            // пересохранение категорий для правильных урлов
            $this->updateCategory();
            Yii::app()->cache->flush();
            ImportFile::model()->updateByPk($file->id, array('status'=>ImportFile::STATUS_COMPLETE));
        } catch (Exception $e) {

            Route::model()->deleteAll('model NOT IN ("Page", "Article")');

            $brands = Brand::model()->findAll();
            foreach ($brands as $brand)
                $brand->save();

            $rootCategories = Category::model()->roots()->findAll();

            foreach ($rootCategories as $root) {
                if ($root->saveNode()) {
                    foreach ($root->descendants()->findAll() as $child) {
                        $child->saveNode();
                    }
                }
            }

            $goods = Good::model()->findAll();
            foreach ($goods as $good) {
                $good->scenario = 'parse';
                $good->colors = Yii::app()->db->createCommand()
                    ->select('color_id')
                    ->from('{{shop_good_color}}')
                    ->where('good_id='.$good->id)
                    ->queryColumn();
                $good->save();
            }

            Yii::app()->cache->flush();

        }

    }


    public function getCategoryId($xmlItem)
    {
        /**@var $category Category*/
        $cat_id = null;
        if(isset($xmlItem->category2->name)&&$xmlItem->category2->name!=''&&!empty($xmlItem->category2->name)){

            if(isset($xmlItem->category2->name)){
                $category = Category::model()->findByAttributes(array('name'=>$xmlItem->category2->name));
                if($category==null){
                    $parentCatModel = null;
                    if(isset($xmlItem->category1->name)){
                        $categoryParent = Category::model()->findByAttributes(array('name'=>$xmlItem->category1->name));
                        if($categoryParent==null){
                            $catParent = new Category;
                            $catParent->name = $xmlItem->category1->name;
                            $catParent->kod = $xmlItem->category1->code;
                            $catParent->type = Category::TYPE_CAT_GOOD;
                            $catParent->saveNode();
                            $catParent->refresh();
                            $parentCatModel = $catParent;
                        }else{
                            $parentCatModel = $categoryParent;
                        }
                    }
                    $model = new Category;
                    $model->name = $xmlItem->category2->name;
                    $model->kod = $xmlItem->category2->code;
                    $model->type = Category::TYPE_CAT_GOOD;
                    $model->parentId = $parentCatModel->id;
                    $model->saveNode();
                    $model->moveAsLast($parentCatModel);
                    $model->refresh();
                    $cat_id= $model->id;
                }else{
                    $category->name = $xmlItem->category2->name;
                    $category->kod = $xmlItem->category2->code;
                    $category->parentId = $category->getParentId();
                    $category->saveNode();
                    $category->refresh();
                    $cat_id = $category->id;
                }
            }
        }elseif(isset($xmlItem->category1->name)){
            $category = Category::model()->findByAttributes(array('name'=>$xmlItem->category1->name));
            if($category==null){
                $model = new Category;
                $model->name = $xmlItem->category1->name;
                $model->kod = $xmlItem->category1->code;
                $model->type = Category::TYPE_CAT_GOOD;
                if($model->saveNode()){
                    $model->refresh();
                    $cat_id= $model->id;
                }else{
                    print_r($model->errors);
                }
            }else{
                $category->name = $xmlItem->category1->name;
                $category->kod = $xmlItem->category1->code;
                $category->saveNode();
                $category->refresh();
                $cat_id = $category->id;
            }
        }
        return $cat_id;

    }
    public function getBrandId($itemXml)
    {
        if (!isset($itemXml->Brend['name']))
            return null;
        $model = Brand::model()->findByAttributes(array(
            'name'=>$itemXml->Brend['name']
        ));
        if ($model === null) {
            $model = new Brand;
            $model->name = $itemXml->Brend['name'];
            $model->save();
            $model->refresh();
        }
        return $model->id;
    }

    public function addGoodImages($xmlItem,$good)
    {
        $oldGoodImages = GoodImage::model()->findAllByAttributes(array(
            'good_id'=>$good->id
        ));
        foreach($oldGoodImages as $oldGoodImage){
            /** @var $oldGoodImage GoodImage */
            $oldGoodImage->delete();
        }
        if (isset($xmlItem->images) && isset($xmlItem->images->image)) {
            foreach ($xmlItem->images->image as $image) {
                $gImage = new GoodImage(); //create new object because image have ordering and use Ordering behavior
                $gImage->good_id=$good->id;
                $gImage->image=$image;
                $gImage->status=GoodImage::STATUS_ACTIVE;
                $gImage->save();
            }
        }

    }

    public function updateCategory()
    {
        $cats = Category::model()->findAll();
        foreach($cats as $cat){
            /**@var $cat Category|NestedSetBehavior*/
            $cat->parentId = $cat->getParentId();
            if($cat->saveNode()){
                if ($cat->getParentId()!==null) {
                    $parent = Category::model()->findByPk($cat->getParentId());
                    $cat->moveAsLast($parent);
                }
            }
            foreach ($cat->attrs as $attr) {
                /**@var $attr Attr*/
                $countAttrGoods = Yii::app()->db->createCommand()
                    ->select('COUNT(id)')
                    ->from(GoodAttrVal::model()->tableName())
                    ->where('attr_id = :attr_id',array(':attr_id'=>$attr->id))
                    ->queryScalar();
                if($countAttrGoods==0){
                    echo $attr->name.' '.$cat->name.'<br>';
                    $attr->delete();
                }
            }
            if($cat->level!=1){
                $countCatGoods = Yii::app()->db->createCommand()
                    ->select('COUNT(id)')
                    ->from(Good::model()->tableName())
                    ->where('category_id = '.$cat->id)
                    ->queryScalar();
                if($countCatGoods==0){
                    $cat->deleteNode();
                }
            }
        }
    }
} 