<?php

class OrderController extends ShopController
{
    /**
     * Add item to shopping card
     */
    public function actionAddToCart()
    {
        if (!isset($_POST['id'])) {
            echo 'fail';
            return;
        }
        $good = Good::model()->findByPk($_POST['id']);
        if (!empty($_POST['price'])){
            $good->variant = ShopGoodVariant::model()->findByPk($_POST['price']);
            $good->variant->price = ShopDiscount::model()->getPriceWithDiscount($good->variant->price);
            $good->id = $good->id."_".$good->variant->id;
        }

        if (isset($_POST['assambler']) && !empty($_POST['assambler'])) {
            $good->assambler = $_POST['assambler'];
        }


        if (!empty($_POST['color'])) {
            $good->color = ShopColor::model()->findByPk($_POST['color']);
        }

        if (isset($_POST['countItem']))
            Yii::app()->shoppingCart->put($good, $_POST['countItem']);
        else
            Yii::app()->shoppingCart->put($good);



        if ($good->goodKit != null) {
            $kitItems = $good->goodKit[0]->getItems();
            foreach ($kitItems as $item) {
                $good1 = $item->good;
                $good1->zeroPrice = true;
                $good1->id = $good1->id."_kit";
                Yii::app()->shoppingCart->put($good1);
            }
        }

        if (!empty($_POST['collectionId']) && $good->goodCollection != null) {
            $collectionItems = $good->goodCollection[0]->getItems();

            foreach ($collectionItems as $item) {
                $good2 = $item->good;
                Yii::app()->shoppingCart->put($good2);
            }

        }

        if (Yii::app()->user->hasState('EShoppingCart'))
            foreach(Yii::app()->getUser()->getState('EShoppingCart') as $data)
                Yii::app()->db->createCommand()->insert("{{analytic_cart}}", array(
                    'session_id'=>Yii::app()->session->sessionID,
                    'item_id'=>$data->id,
                    'variant_id'=>$data->variant != null ? $data->variant->id : '',
                    'color_id'=>$data->color != null ? $data->color->id : ''
                ));
        $result = array();
        $result['itemCount'] = Yii::app()->shoppingCart->getItemsCount();
        $result['totalSum'] = Yii::app()->shoppingCart->getCost();
        echo json_encode(true);
        Yii::app()->end();
    }

    /**
     * Set good quantity in shopping card
     */
    public function actionSetGoodQuantity()
    {
        if (!isset($_POST['good_id']) || !isset($_POST['quantity'])) {
            echo 'fail';
            return;
        }

        $good = Good::model()->findByPk($_POST['good_id']);
        if (isset($_POST['ass_price']))
            $good->assambler = $_POST['ass_price'];
        if (isset($_POST['variant_id']) && !empty($_POST['variant_id'])) {
            $good->variant = ShopGoodVariant::model()->findByPk($_POST['variant_id']);
            $good->variant->price = ShopDiscount::model()->getPriceWithDiscount($good->variant->price);
            $good->id = $good->id."_".$good->variant->id;
        }


        if ($good !== null)
            Yii::app()->shoppingCart->update($good, $_POST['quantity']);
        Yii::app()->end();
    }

    /**
     * get items count in shopping card
     */
    public function actionGetCartQuantity()
    {
        $count = Yii::app()->shoppingCart->getItemsCount();
        $cost = CAlexHelper::GetMoneyFormat(Yii::app()->shoppingCart->getCost());
        echo $count . ',' . $cost;
    }

    /**
     * Delete item from shopping card
     */
    public function actionDeleteItem()
    {
        if (!isset($_GET['good_id'])) {
            echo 'fail';
            return;
        }
        Yii::app()->shoppingCart->remove($_GET['good_id']);
        echo json_encode(array(
            'itemCount'=>Yii::app()->shoppingCart->getItemsCount(),
            'totalSum' => Yii::app()->shoppingCart->getCost(),
            'cartGoods' => $this->renderPartial('shopping-cart-ajax', array('items'=>Yii::app()->shoppingCart), true)
        ));
        return true;
        Yii::app()->end();

    }

    /**
     * Render shopping card page
     */
    public function actionIndex()
    {
        $this->title = "Корзина";
        $data = array();
        foreach (Yii::app()->shoppingCart->getPositions() as $position)
            $data[] = $position;
        $dataProvider = new CArrayDataProvider($data, array(
            'pagination'=>false
        ));

        $this->render('shopping-cart', array(
                'dataProvider'=>$dataProvider
            )
        );
    }

    public function actionUpdateCartSum()
    {
        $cost = (string)Yii::app()->shoppingCart->getCost();
        echo json_encode(CAlexHelper::GetMoneyFormat($cost));
        Yii::app()->end();
    }

    public function actionUpdateCountItemsBasket()
    {
        $count = (string)Yii::app()->shoppingCart->getCount();
        echo json_encode($count);
        Yii::app()->end();
    }

    public function actionOrdering()
    {
        Yii::import('application.modules.profile.models.*');
        if (Yii::app()->shoppingCart->isEmpty())
            $this->redirect(array('/shop/order/index'));
        if(!empty(Yii::app()->params['success_massage'])){
            $success_massage = Yii::app()->params['success_massage'];
        }else{$success_massage = 'Заказ успешно обработан! В ближайшее время наши менеджеры свяжутся с Вами.';}
        $order = new Order('site_order');
        if (Yii::app()->user->isGuest) {
            $user = new User('newClient');
            $profile = new Profile('newClient');
            if(isset($_POST['User'])&&!empty ($_POST['User']['email'])){
                $newUser = User::model()->findByAttributes(array(),'email = :email',array(':email'=>($_POST['User']['email'])));
                if(!empty($newUser)){
                    $user = $newUser;
                }else{
                   $user = new User('newClient');
                }
            }
            $this->performAjaxValidation(array($user));
            $this->performAjaxValidation(array($profile));
            if (isset($_POST['User'])&&isset($_POST['Profile'])&&isset($_POST['Order'])) {
                $user->attributes = $_POST['User'];
                $user->save();
                User::model()->sendEmailReg($_POST['User']['email'],$_POST['User']['repeat_password']);
                $profile->attributes = $_POST['Profile'];
                $profile->user_id = $user->id;
                $profile->save();
                $order->attributes = $_POST['Order'];
                $order->user_id = $user->id;
                if ($order->save()) {
                    Yii::app()->user->setFlash('success_order', $success_massage);
                }else{
                    Yii::app()->user->setFlash('error_order', 'При добавлении заказа возникла ошибка! Повторите свою попытку через некоторое время');
                }
                $this->render('order_success');
                Yii::app()->end();
            }
        }else{
            $user_id = Yii::app()->user->id;
            $user = User::model()->findByPk($user_id);
            $user->scenario = 'ordering';
            $profile = $user->profile;
            $profile->scenario = 'ordering';
            $this->performAjaxValidation(array($user,$profile));
            if (isset($_POST['User'])&&isset($_POST['Profile'])&&isset($_POST['Order'])) {
                $user->attributes = $_POST['User'];
                $profile->attributes = $_POST['Profile'];
                $order->attributes = $_POST['Order'];
                if (!empty($_POST['shipping'])) {
                    $order->shipping_id = $_POST['shipping'];
                }
                $order->user_id = Yii::app()->user->id;
                if ($user->save() && $profile->save() && $order->save()) {

                    Yii::app()->user->setFlash('success_order',$success_massage);
                }else{
                    Yii::app()->user->setFlash('error_order', 'При добавлении заказа возникла ошибка! Повторите свою попытку через некоторое время');
                }
                $this->render('order_success');
                Yii::app()->end();
            }
        }
        $data = array();
        foreach (Yii::app()->shoppingCart->getPositions() as $position)
            $data[] = $position;
        $dataProvider = new CArrayDataProvider($data);
        $this->render('ordering', array(
                'dataProvider'=>$dataProvider,
                'user'=>$user,
                'order'=>$order,
                'profile'=>$profile,
            )
        );
    }

    public function actionPayment($order_id)
    {
        $order = Order::model()->findByPk($order_id);
        if ($order !== null) {
            $order->date_payment = Yii::app()->dateFormatter->format('yyyy-MM-dd HH:mm:ss', time());
            $order->status = Order::STATUS_PAYMENT;
            if ($order->save())
                $this->render('payment', array('message'=>'Ваш заказ успешно оплачен!'));
            else
                $this->render('payment', array('message'=>'Ошибка при оплате заказа!'));
        } else
            throw new CHttpException(404, 'Не верный параметр оплаты!');
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']))
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Render shopping card
     */
    public function actionCartAjax()
    {
        $this->renderPartial('shopping-cart-ajax', array());
    }
}
?>
