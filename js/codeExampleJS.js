$(document).on('click', '.addSum', function () {
    var this_ = $(this),
    id = this_.attr('data-user'),
    name = this_.attr('data-userName'),
    ball = this_.attr('data-userBall'),
    overdraft = this_.attr('data-useroverdraft'),
    transaction_id = this_.attr('data-transaction_id');
    $('.bal_cl_id').val(id);
    $('.bal_client').val('');
    $('.over_cl_id').val(overdraft);
    $('.transaction_cl_id').val(transaction_id);
    $('.cl_overdraft').text(overdraft);
    $('.cl_name').text(name+' ');
    $('.cl_bal').text(ball);
    });
    $(document).on('click', '.balance_client_but', function () {
        $('#balanceForm').submit();
    });

function headerOutBalanceClientModalForm(title,action,typeForm,name,ball,transaction_id,user_id,grid_id){
    $.post(
        action,
        {typeForm:typeForm},
        function (form) {
            var modal = $('#outBalanceAdmin');
            modal.find('div.modal-body').html(form);
            modal.find('h4.title_header').text(title);
            modal.find('#outBalAdminSubmitButton').attr('data-transaction_id',transaction_id);
            modal.find('#outBalAdminSubmitButton').attr('data-user_id',user_id);
            modal.find('#outBalAdminSubmitButton').attr('data-grid_id',grid_id);
            $('.cl_name').text(name+' ');
            $('.cl_bal').text(ball);
            modal.modal('show');
        }
    )
}
function showMessageClient(title,html){
    var modal = $('#balClientMessage');
    modal.find('div.modal-body').html(html);
    modal.find('h4.title_header').text(title);
    modal.modal('show');
    }
function adminOutBalanceClient(outBallClient){
    outBallClient.on('click',function(e){
        e.preventDefault();
        var title = 'Списание суммы',
            action = '/user/changeBalanceAdminForm',
            typeForm = 'outAdminBal';
        var this_ = $(this),
            id = this_.attr('data-user'),
            name = this_.attr('data-userName'),
            ball = this_.attr('data-userBall'),
            transaction_id = this_.attr('data-transaction_id'),
            grid_id = this_.attr('data-grid_id');
        headerOutBalanceClientModalForm(title,action,typeForm,name,ball,transaction_id,id,grid_id);
    });
}
$(document).ready(function(){
    var outBallClient = $('a.outSum');
    adminOutBalanceClient(outBallClient);
});



$(document).on('click','#outBalAdminSubmitButton',function(e){
        e.preventDefault();
        var modal = $('#outBalanceAdmin'),
        buttonSubmit = $('#outBalAdminSubmitButton'),
        transaction_id = buttonSubmit.attr('data-transaction_id'),
        user_id = buttonSubmit.attr('data-user_id'),
        form = $('#outBalAdmin-form').serializeArray(),
        grid_id = buttonSubmit.attr('data-grid_id') ;
        modal.modal('hide');
        form.push({name: 'type_operation',value: 'outAdminBalance'});
        form.push({name: 'transaction_id',value: transaction_id});
        form.push({name: 'user_id',value: user_id});
    var data = jQuery.param(form);
    $.post(
        '/user/changeBalanceAdmin',
         data,
        function(result){
            var res = JSON.parse(result);
            if(res=='success'){
                $.fn.yiiGridView.update(grid_id);
                var message = '<p>Списание произошло успешно! Деньги были списаны с баланса клиента.</p>',
                title = 'Сообщение!';
                showMessageClient(title,message);
            }else if(res=='error'){
                $.fn.yiiGridView.update(grid_id);
                var messageEr = '<p>Произошла непредвиденная ошибка. Попробуйте повторить позже.</p>',
                titleEr = 'Ошибка!';
                showMessageClient(titleEr,messageEr);
            }
        }
    )
});
//change font-size in we_rec block afterAjaxUpdate
function weRecSliderTextSize()
{
    var we_rec_title = $('p.title_we'),
        we_rec_slides = $('.slides_we'),
        length_we_rec_title = we_rec_slides.find(we_rec_title).length;
    for (var  i = 0; i<=length_we_rec_title;i++)
    {
        var p = we_rec_slides.find(we_rec_title).eq(i),
            count_we_rec_title = p.text().length;
        if( count_we_rec_title > 33)
        {
            p.css({ "font-size": "11px"});

        }else if( count_we_rec_title > 18)
        {
            p.css({ "font-size": "12px"});

        }else if(count_we_rec_title > 11)
        {
            p.css({ "font-size": "15px", 'height':'14px'});
        }
    }
}

   $(document).on('submit','#callback-form-form',function(e){
        e.preventDefault();
        var th = $(this);
        var mess =$('p.messageSuccess');
        $.post(
            "<?php echo Yii::app()->controller->createUrl('/callbackForm/create');?>",
            th.serialize(),
            function (result) {
                if(result == 'ok'){
                    mess.fadeIn(500).delay(5000).fadeOut(700);
                    th[0].reset();
                }
            }
        );
    });
/////////////////////////////////////////////////////////    
function centeredImageInParentBlock(image,parent)
{
    //parent must have static height
    image.one('load', function() {
        // do stuff
        var $this = $(this);
        var parentHeight = $this.parents(parent).height();
        var imgHeight = $this.height();
        var mar = (parentHeight-imgHeight)/2;
        $this.css({marginTop:mar+'px'});
    });
}
centeredImageInParentBlock($("img.goodImage"),'.goodLink');
/////////////////////////////////////////////////////////////////
function addClassLastElInRow(parentEl,childrenEl,startEl,countFor){
    $(function() {
        var count_items = $(parentEl).children(childrenEl).length;
        for (var i = startEl; i<=count_items; i+=countFor){
            $(parentEl).children(childrenEl).eq(i).addClass('lastInRow');
        }
    });
}
addClassLastElInRow('.categoryItems','div.item',2,3);
//////////////////////////////////////////////////////////////////////
<script type="text/javascript">
    $(document).on('click','div.havka>a',function(e){
        e.preventDefault();
        var $_this=$(this),
            url = '/kitchen/menuGood/getGoodAjax',
            goodID = $_this.data('id'),
            language = '<?php echo Yii::app()->language?>';
        $.ajax({
                type:'POST',
                dataType : "json",
                data:{good_id:goodID,lang:language},
                url:url,
                success: function (data, textStatus) { // вешаем свой обработчик на функцию success

                    $('.goodViewBlock').html(data.html);
                    location.href = data.goodUrl;
                    }
            }
        );
    });
</script>