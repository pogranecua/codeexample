<?php
/* @var $this ProfileController */
/* @var $model Profile */
?>
<?php $this->title = Yii::t('blog/admin', 'Update Profile').' '.$model->id; ?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>