<?php
/**
 * Created by PhpStorm.
 * User: Sergiy
 * Date: 09.09.13
 * Time: 11:59
 */
/* @var $this ProfileController */
/* @var $model UserFiles */


?>

<?php $this->title = Yii::t('core/profile', 'Manage User Files'); ?>



<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'user-files-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'name',
        'description',
        array(
            'header'=>'Автор',
            'name'=>'author_id',
            'value'=>'$data->userName',
        ),
        'date_add',
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{delete}',//{update}
            'buttons'=>array(
                'delete'=>array(
                    'url'=>'Yii::app()->createUrl("/profile/admin/profile/deleteFile",array("id"=>$data->id))',
                ),
            ),
        ),
    ),
)); ?>