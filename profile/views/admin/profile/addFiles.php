<?php
/**
 * Created by PhpStorm.
 * User: Sergiy
 * Date: 09.09.13
 * Time: 11:55
 */
?>
<?php
/* @var $this ProfileController */
/* @var $model UserFiles */
/* @var $form CActiveForm */
?>

<?php $this->title = Yii::t('core/profile', 'Create UserFiles'); ?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'user-files-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'file_name'); ?>
        <?php echo $form->fileField($model,'file_name',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'file_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'author_id'); ?>
        <?php echo $form->dropDownList($model,'author_id', CHtml::listData(User::model()->findAll(), 'id', 'username')); ?>
        <?php echo $form->error($model,'author_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'type_access'); ?>
        <?php echo $form->dropDownList($model,'type_access',UserFiles::$sel_type_access,
                array(
                    'ajax' => array(
                        'type'=>'POST', //request type
                        'url'=>Yii::app()->createUrl("/profile/admin/profile/ajaxSelectType"), //url to call.
                        'update'=>'#sel_user', //selector to update
                        //'data'=>'js:jQuery(this).parents("form").serialize()'
                    )));
                ?>
        <?php echo $form->error($model,'type_access'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'users_id'); ?>
        <?php echo $form->dropDownList($model,'users_id',array(0=>'Для начала выберите тип доступа'),array('id'=>'sel_user','multiple'=>true,)); ?>
        <?php echo $form->error($model,'users_id'); ?>
    </div>



    <div class="row">
        <?php echo $form->labelEx($model,'date_add'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,
            'attribute'=>'date_add',
            'options'=>array(
                'dateFormat'=>'dd-mm-yy'
            ),
        )); ?>
        <?php echo $form->error($model,'date_add'); ?>
    </div>

    <div class="row buttons">
        <?php
        $this->widget('bootstrap.widgets.TbButton',array(
            'label' => ($model->isNewRecord ? Yii::t('core/admin', 'Create') : Yii::t('core/admin', 'Save')),
            'buttonType' => 'submit',
            'type' => 'primary',
        )); ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton',array(
            'label' => Yii::t('core/admin', 'Apply'),
            'buttonType' => 'submit',
            'type' => 'primary',
            'htmlOptions' => array(
                'name'=>'apply'
            ),
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->