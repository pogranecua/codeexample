<?php
/* @var $this ProfileController */
/* @var $model Profile */
?>

<?php $this->title = Yii::t('blog/admin', 'Create Profile'); ?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>