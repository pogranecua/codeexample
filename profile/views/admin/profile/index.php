<?php
/* @var $this ProfileController */
/* @var $model Profile */

?>
<?php $this->title = Yii::t('profile/admin', 'Управление профилями');
$this->widget('bootstrap.widgets.TbGridView', array(
'type'=>'striped bordered condensed',
'id'=>'profile-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
        array(
            'name'=>'user_id',
            'value'=>'$data->user !== null ? $data->user->username : "клиент ".$data->user_id'
        ),
		'social_link',
        array(
            'name'=>'sex',
            'value'=>'$data->sexText',
            'filter'=>CHtml::activeDropDownList($model,'sex', Profile::$sex_list, array('empty'=>'------')),
        ),
		'date_birth',
		'address',
		'phone',
        array(
            'header'=>'Страна',
            'value'=>'$data->country_ !==null ? $data->country_->name : " "'
        ),
        array(
            'header'=>'Регион',
            'value'=>'$data->region_ !==null ? $data->region_->name : " "'
        ),
        array(
            'header'=>'Город',
            'value'=>'$data->city_ !==null ? $data->city_->name : " "'
        ),
        array(
            'header'=>'Email',
            'value'=>'$data->user !== null ? $data->user->email : "no email"'
        ),
        array(
            'header'=>'Роль',
            'value'=>'$data->user !== null ?$data->user->role : "клиент "',
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}{delete}'
        ),
    ),
)); ?>
