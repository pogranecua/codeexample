<?php
/**
 * Created by PhpStorm.
 * User: Sergiy
 * Date: 05.09.13
 * Time: 15:23
 * @var $this DefaultController
 * @var $model Good
*/
$this->title = Yii::t('core/profile','favoriteGoods');
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'user-favorite-goods',
    'dataProvider'=>$model,

    'columns'=>array(
        array(
            'header'=>'Название',
            'name'=>'name'
        ),
        array(
            'header'=>'Производитель',
            'value'=>'$data->brand->name',
            'type'=>'raw',
        ),
        array(
            'header'=>'Категория',
            'value'=>'$data->category->name',
            'type'=>'raw',
        ),
        array(
            'header'=>'Картинка',
            'value'=>'$data->getGoodImage("xs")',
            'type'=>'raw',
        ),
        array(
            'class'=>'CButtonColumn',
            'header'=>'Действия',
            'template'=>' {delete} {view} ',
            'buttons'=>array(
                'delete'=>array(
                    'label'=>'Удальть товар из избраного?',
                    'imageUrl'=>'/images/deletered24x24.png',
                    'url'=>'Yii::app()->createUrl("/profile/default/deleteGoods",array("id"=>$data->id))',
                    'options'=>array('class'=>'delete_goods')
                ),
                'view'=>array(
                    'label'=>'Просмотр товара',
                    'imageUrl'=>'/images/information.png',
                    'url'=>'Yii::app()->createUrl("/shop/good/view", array( "seo_link"=>$data->category->alias, "alias"=>$data->alias))',
                    'options'=>array('class'=>'view_goods')
                ),
            ),
        ),
    ),
));