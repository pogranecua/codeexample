<?php

/**
 * This is the model class for table "{{profile}}".
 *
 * The followings are the available columns in table '{{profile}}':
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $social_link
 * @property integer $city_id
 * @property integer $sex
 * @property string $date_birth
 * @property string $avatar
 * @property string $address
 * @property string $last_date_send_mail
 *
 * @property integer $phone
 *  @property string $country
 *  @property string $region
 *  @property string $city
 */
class Profile extends CActiveRecord
{
    const SEX_MAN = 1;
    const SEX_WOMAN = 2;

    const PERIOD_SEND_MAIL_ANY_DAY = 1;
    const PERIOD_SEND_MAIL_3_DAY = 2;
    const PERIOD_SEND_MAIL_WEEK = 3;

    const STATUS_SUBSCRIBE = 1;
    const STATUS_UNSUBSCRIBE = 0;

    public static $sex_list = array(
        self::SEX_MAN=>'Мужской',
        self::SEX_WOMAN=>'Женский'
    );

    public $day_birth;
    public $month_birth;
    public $year_birth;

    public $subscribeCats = array();

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Profile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{profile}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id, address, phone, city, country, region', 'required','on'=>'registration'),
			array('user_id, phone, sex, day_birth, month_birth, year_birth, status_subscribe, period_send_mail', 'numerical', 'integerOnly'=>true),
			array('social_link, date_birth, last_date_send_mail', 'length', 'max'=>128),
            array('address, subscribeCats', 'safe'),

            //userUpdate
            array('address, phone, city, country, region', 'required', 'on'=>'userUpdate'),
            //ordering
            array('address, phone, city, country, region', 'required', 'on'=>'ordering'),
            //ordering
            array('phone, address  ', 'safe', 'on'=>'newClient'),
//            array('phone, city, country, region ', 'required', 'on'=>'newClient'),
//            array('address', 'required', 'on'=>'newClient','message'=>'Необходимо заполнить поде Адресса доставки'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id,social_link, sex, date_birth, phone ,city, country, region', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
            'country_'=>array(self::BELONGS_TO, 'GeoCountry', 'country'),
            'region_'=>array(self::BELONGS_TO, 'GeoRegion', 'region'),
            'city_'=>array(self::BELONGS_TO, 'GeoCity', 'city'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'name' => 'Имя',
			'social_link' => 'Профиль в социальной сети',
			'sex' => 'Пол',
			'date_birth' => 'Дата рождения',
			'address' => 'Адрес',
			'addressShipping' => 'Адрес доставки',
            'phone' => Yii::t('core/profile','phone'),
            'country' => Yii::t('core/profile','country'),
            'region' => Yii::t('core/profile','region'),
            'city' => Yii::t('core/profile','city'),
		);
	}

    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if (!empty($this->year_birth) && !empty($this->month_birth) && !empty($this->day_birth))
                $this->date_birth = $this->year_birth.'-'.$this->month_birth.'-'.$this->day_birth;
            $this->date_birth = Yii::app()->dateFormatter->format('yyyy-MM-dd', $this->date_birth);
            return true;
        } else
            return false;
    }

    public function afterSave()
    {
        parent::afterSave();
        if ($this->scenario == 'registration') {
            Yii::import('application.modules.shop.models.*');
            $categories = Category::model()->findAll();
            foreach ($categories as $cat)
                Yii::app()->db->createCommand()->insert('{{profile_subscribe_cat}}', array(
                    'user_id'=>$this->user_id,
                    'cat_id'=>$cat->id
                ));
        }
        if ($this->scenario == 'userUpdate') {
            Yii::app()->db->createCommand()->delete('{{profile_subscribe_cat}}', 'user_id = :user_id', array(':user_id'=>Yii::app()->user->id));
            foreach ($this->subscribeCats as $key=>$value)
                if ($value)
                    Yii::app()->db->createCommand()->insert('{{profile_subscribe_cat}}', array(
                        'user_id'=>Yii::app()->user->id,
                        'cat_id'=>$key
                    ));
        }
    }

    public function beforeDelete()
    {
        if(parent::beforeDelete()){
            Yii::app()->db->createCommand()->delete("{{profile_subscribe_cat}}",'user_id = :user_id',array(':user_id'=>$this->user_id));
            return true;
        }else return false;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->day_birth = (integer) Yii::app()->dateFormatter->format('dd', $this->date_birth);
        $this->month_birth = (integer) Yii::app()->dateFormatter->format('MM', $this->date_birth);
        $this->year_birth = (integer) Yii::app()->dateFormatter->format('yyyy', $this->date_birth);
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('social_link',$this->social_link,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('date_birth',$this->date_birth,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getSexText()
    {
        if (isset(self::$sex_list[$this->sex]))
            return self::$sex_list[$this->sex];
    }

    public static function getDayList()
    {
        $days = array();
        for ($i = 1; $i <=31; $i++)
            $days[$i] = $i;
        return $days;
    }

    public static function getMonthList()
    {
        return array (
            1 => "Январь",
            2 => "Февраль",
            3 => "Март",
            4 => "Апрель",
            5 => "Май",
            6 => "Июнь",
            7 => "Июль",
            8 => "Август",
            9 => "Сентябрь",
            10 => "Октябрь",
            11 => "Ноябрь",
            12 => "Декабрь"
        );
    }

    public static function getYearsList()
    {
        $currentYear = (integer) Yii::app()->dateFormatter->format('yyyy', time());
        $years = array();
        for ($i = 1960; $i <= $currentYear; $i++)
            $years[$i] = $i;
        return $years;
    }

    public static function isSubscribeCat($id)
    {
        $res = Yii::app()->db->createCommand()
            ->select('*')
            ->from('{{profile_subscribe_cat}}')
            ->where('cat_id = :cat_id AND user_id = :user_id', array(':cat_id'=>$id, ':user_id'=>Yii::app()->user->id))
            ->queryAll();
        return (!empty($res));
    }

    public function getCitySelect()
    {
        $result=array();
        $result[0]='Выберите город';
        $city=Yii::app()->db->createCommand()
            ->select('*')
            ->from('{{geo_city}}')
            ->where('countryid = :country AND regionid = :region',array(':country'=>$this->country,':region'=>$this->region))
            ->queryAll();
        foreach($city as $items)
        {
            $result[$items['id']] = $items['name'];
        }
        return $result;
    }
}