<?php

/**
 * This is the model class for table "{{user_files}}".
 *
 * The followings are the available columns in table '{{user_files}}':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $file_name
 * @property integer $author_id
 * @property string $date_add
 *
 * @property UserFiles $usersFile
 */
class UserFiles extends CActiveRecord
{
    private $_oldFile;

    public $role;
    public $users_id;
    public $type_access;


    const FOR_GROUP = 2;
    const FOR_USERS = 1;

    public static $sel_type_access = array(
        0=>'Выберите тип',
        self::FOR_GROUP=>'Для групы',
        self::FOR_USERS=>'Для пользователей'
    );


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserFiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_files}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('author_id', 'numerical', 'integerOnly'=>true),
			array('name, description, file_name', 'length', 'max'=>128),
			array('type_access, users_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, file_name, author_id, date_add', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'usersFile' => array(self::MANY_MANY, 'UserFiles', '{{files_access}}(file_id,type_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя файла',
			'description' => 'Описание',
			'file_name' => 'Файл',
			'author_id' => 'Автор',
			'date_add' => 'Дата создания',
			'users_id' => 'Кому',
			'type_access' => 'Тип',
		);
	}

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord) {
                $this->author_id = Yii::app()->user->id;
            }
            $this->date_add = Yii::app()->dateFormatter->format("yyyy-MM-dd HH:mm:ss",$this->date_add);
            $this->file_name = CUploadedFile::getInstance($this,'file_name');
            if($this->file_name) {
                $fileName = uniqid();
                $name_old = $this->file_name->getName();
                $fileExtension = $this->file_name->getExtensionName();
                $dir = Yii::getPathOfAlias('webroot.images.upload.usersFile').DIRECTORY_SEPARATOR;
                $this->file_name->saveAs($dir.$fileName.'_'.$name_old);
                $this->file_name = $fileName.'_'.$name_old;

            } elseif (!empty($this->_oldFile)) {
                $this->file_name = $this->_oldFile;
            }
            return true;
        }else
            return false;

    }

    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            Yii::app()->db->createCommand()->delete('{{files_access}}','file_id = :file',array(':file'=>$this->id));
            return true;
        } else
            return false;
    }
    public function afterDelete()
    {
        parent::afterDelete();


    }
    public function afterFind()
    {
        parent::afterFind();
        $this->_oldFile= $this->file_name;
    }
    public function afterSave()
    {
        parent::afterSave();
        if($this->type_access == self::FOR_GROUP)
        {
                $criteria = new CDbCriteria();
                $criteria->addInCondition('role',$this->users_id);
                $users= User::model()->findAll($criteria);
            foreach($users as $user)
            {
                Yii::app()->db->createCommand()->insert('{{files_access}}',array('type'=>$user->role,'type_id'=>$user->id,'file_id'=>$this->id));
            }
        }else{
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id',$this->users_id);
            $users= User::model()->findAll($criteria);
            foreach($users as $user)
            {
                Yii::app()->db->createCommand()->insert('{{files_access}}',array('type'=>$user->role,'type_id'=>$user->id,'file_id'=>$this->id));
            }
        }
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('date_add',$this->date_add,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getUserName()
    {
        Yii::import('application.models.User');
        $user = User::model()->findByPk($this->author_id);
        return $user->username;
    }
}