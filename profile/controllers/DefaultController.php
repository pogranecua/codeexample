<?php
/**
 * Created by PhpStorm.
 * User: Sergiy
 * Date: 27.08.13
 * Time: 11:26
 */

class DefaultController extends CmsController
{
    public function actionUpdate()
    {
        $this->layout='//layouts/profile';
        Yii::import('application.models.*');
        if (Yii::app()->user->isGuest)
            throw new CHttpException(403,'У вас недостаточно прав для выполнения указанного действия.');
        $user = User::model()->findByPk(Yii::app()->user->id);
        $profile = Profile::model()->findByAttributes(array('user_id'=>$user->id));

        if ($user === null) {
            $session=new CHttpSession;
            $session->open();
            $session->clear();
            $session->close();
            Yii::app()->request->cookies->clear();
            $this->redirect(array('/site/index'));
        }

        $user->scenario = 'userUpdate';
        $profile->scenario = 'userUpdate';
        $this->performAjaxValidation(array($user,$profile));
        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
            $profile->attributes = $_POST['Profile'];
            if ($user->save() && $profile->save())
                Yii::app()->user->setFlash('success_update','Даные успешно обновлены!!!');
        }

        $this->render('update', array(
            'user'=>$user,
            'profile'=>$profile,
        ));
    }
    //selectbox region on registration and update profile
    public function actionAjaxRegion()
    {
        $criteria = new CDbCriteria();
        if (isset($_POST['country']))
            $criteria->compare('countryid', $_POST['country']);
        /* @var GeoRegion[] $models */
        $models = GeoRegion::model()->findAll($criteria);
        $result = array();
        $result[0] = 'Выбрать';
        foreach ($models as $model)
            $result[$model->id] = $model->name;
        echo json_encode($result);
        Yii::app()->end();

    }
    //selectbox city on registration and update profile
    public function actionAjaxCity()
    {
        $criteria = new CDbCriteria();
        if (isset($_POST['region']))
            $criteria->compare('regionid', $_POST['region']);
        $models = GeoCity::model()->findAll($criteria);
        $result = array();
        $result[0] = 'Выбрать';
        foreach ($models as $model)
            $result[$model->id] = $model->name;
        echo json_encode($result);
        Yii::app()->end();
    }
    // user favorite goods
    public function actionFavoriteGoods()
    {
        $this->layout='//layouts/profile';
        Yii::import('application.modules.shop.models.*');
        $user_id=Yii::app()->user->id;
        if($user_id!==null)
        {
            $user = User::model()->findByPk($user_id);
            $favoriteGoods = $user->favoriteGoods;
            $dataProvider = new CArrayDataProvider($favoriteGoods,array('pagination'=>array('pageSize'=>20)));
            $this->render('favoriteGoods',array(
                'model'=>$dataProvider,
            ));
        }else{
            throw new CHttpException(403,'Авторизируйтесь на сайте.');
        }

    }
    // user fails for user
    public function actionFiles()
    {
        $this->layout='//layouts/profile';

        $user_id=Yii::app()->user->id;
        if($user_id!==null)
        {
            $user = User::model()->findByPk($user_id);
            $fails = $user->userFiles;
            $dataProvider = new CArrayDataProvider($fails);
            $this->render('files',array(
                'model'=>$dataProvider,
            ));
        }else{
            throw new CHttpException(403,'Авторизируйтесь на сайте.');
        }

    }

    public function actionGetFile($fileName = null) {
        if ($fileName !== NULL) {
            // некоторая логика по обработке пути из url в путь до файла на сервере
            $currentFile = Yii::getPathOfAlias('webroot.images.upload.usersFile').DIRECTORY_SEPARATOR.$fileName;
            if (is_file($currentFile)) {
                header("Content-Type: application/octet-stream");
                header("Accept-Ranges: bytes");
                header("Content-Length: " . filesize($currentFile));
                header("Content-Disposition: attachment; filename=".$fileName);
                readfile($currentFile);
            };
        } else {
            $this->redirect('куда переправляем юзера в случае ошибки');
        };
    }
    /*
     * action for delete users favorite goods
     * */
    public function actionDeleteGoods($id)
    {
        $user_id=Yii::app()->user->id;
         Yii::app()->db->createCommand()->delete('{{favoriteGoods}}','user_id = :user AND good_id = :id',array(':user'=>$user_id,':id'=>$id));
    }
    /*show user orders
     * */
    public function actionOrders()
    {
        $this->layout='//layouts/profile';

        $user_id=Yii::app()->user->id;
        $orders = Order::model()->findAllByAttributes(array('user_id'=>$user_id,'user_status'=>Order::USER_NO_DELETE));
        $dataProvider = new CArrayDataProvider($orders,array(
            'pagination'=>array(
                'pageSize'=>50
            )
        ));

        $this->render('orders',array('dataProvider'=>$dataProvider));

    }
    /*change status user order (user del order from profile)
     * */
    public function actionDeleteOrder($id)
    {
        Order::model()->updateByPk($id,array('user_status'=>Order::USER_DELETE));
    }
    /*view user order items
     * */
    public function actionViewOrder($id)
    {
        $this->layout='//layouts/profile';
        $items = OrderItem::model()->findAllByAttributes(array('order_id'=>$id));
        $dataProvider = new CArrayDataProvider($items,array(
            'pagination'=>array(
                'pageSize'=>50
            )
        ));

        $this->render('ordersItems',array('dataProvider'=>$dataProvider));
    }

    protected function performAjaxValidation($models)
    {
        if(isset($_POST['ajax']))
        {
            echo CActiveForm::validate($models);
            Yii::app()->end();
        }
    }
} 