<?php

class ProfileController extends CmsAdminController
{

    public function accessSpecs()
    {
        return array(
            'operations'=>array(
                'index'=>'Список записей',
                'update'=>'Редактирование записи',
                'create'=>'Создание записи',
                'delete'=>'Удаление записи',
            ),
        );
    }

    public $menuTitle = "Модуль профиль";
    public $menu = array(
        array('label'=>'Управление записями', 'url'=>array('index')),
        array('label'=>'Управление файлами', 'url'=>array('managerFiles')),
    );
    public $subMenuTitle = "Действия";
    public $subMenu = array(
        array('label'=>'Добавить запись', 'url'=>array('create')),
        array('label'=>'Добавить файл', 'url'=>array('addFile')),
    );


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Profile;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Profile']))
		{
			$model->attributes=$_POST['Profile'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Profile']))
		{
			$model->attributes=$_POST['Profile'];
			if($model->save())
				$this->redirect(array('index'));
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
        $model=new Profile('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Profile']))
        $model->attributes=$_GET['Profile'];

        $this->render('index',array(
        'model'=>$model,
        ));
	}
    /**
	 * Manages files for user.
	 */
	public function actionManagerFiles()
	{
        $model=new UserFiles('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['UserFiles']))
        $model->attributes=$_GET['UserFiles'];

        $this->render('managerFiles',array(
        'model'=>$model,
        ));
	}
    /**
	 * add file for users.
	 */
	public function actionAddFile()
	{
        $model=new UserFiles;
        $model->date_add = date("d-m-Y");
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset($_POST['UserFiles']))
        {
            $model->attributes=$_POST['UserFiles'];
            if($model->save())
                $this->redirect(array('managerFiles'));
        }

        $this->render('addFiles',array(
            'model'=>$model,
        ));
	}
    /**
	 * add file for users.
	 */
	public function actionDeleteFile($id)
	{
        $file= UserFiles::model()->findByPk($id);
        $file->delete();
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

    //selectbox users or group add files
    public function actionAjaxSelectType()
    {
        $post = $_POST['UserFiles']['type_access'];
        if(isset($post))
            if(!empty($post))
                if($post == UserFiles::FOR_GROUP)
                {
                    $roles = CHtml::listData(Yii::app()->authManager->roles, 'name', 'description');
                    foreach($roles as $value=>$name)
                    {
                    echo CHtml::tag('option',
                        array('value'=>$value),CHtml::encode($name),true);
                    }
                }
                elseif($post == UserFiles::FOR_USERS)
                {
                    $result = array();
                    $result[0]='Выбрать';
                    $users = User::model()->findAll();
                    foreach ($users as $user)
                    {
                    $result[$user->id] = $user->username;
                    }
                    foreach($result as $value=>$name)
                    {
                        echo CHtml::tag('option',
                            array('value'=>$value),CHtml::encode($name),true);
                    }
                }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Profile the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Profile::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Profile $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
