<?php
return array(
    'order'=>'1',
    'rules'=>array(
    '/profile/favoriteGoods'=>'/profile/default/favoriteGoods',
    '/profile/files'=>'/profile/default/files',
    '/profile/orders'=>'/profile/default/orders',
    '/profile/viewOrder'=>'/profile/default/viewOrder',
    '/profile/deleteOrder'=>'/profile/default/deleteOrder',
    '/profile/getFile'=>'/profile/default/getFile',
//    '/images/upload/usersFile/<file_name:\w+>'=>'images/upload/usersFile/',
    '/profile/deleteGoods'=>'/profile/default/deleteGoods'
    )
);