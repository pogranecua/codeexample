<?php
return array(
    'title'=>'Profile',
    'name'=>'profile',
    'description'=>'Profile module',
    'version'=>'0.0.1',
    'author'=>array(
        'url'=>'http://web-logic.biz',
        'name'=>'Web-logic',
        'email'=>'i@web-logic.biz',
    ),
    'admin_controller'=>'/admin/profile/index',
);
