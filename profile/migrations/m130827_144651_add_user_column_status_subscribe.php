<?php

class m130827_144651_add_user_column_status_subscribe extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{user}}', 'status_subscribe', 'TINYINT(1) NOT NULL DEFAULT 1');
	}

	public function down()
	{
	}
}