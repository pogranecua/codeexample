<?php

class m130827_081832_drop_column_name extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('{{profile}}', 'name');
	}

	public function down()
	{
		echo "m130827_081832_drop_column_name does not support migration down.\n";
		return false;
	}
}