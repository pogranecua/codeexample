<?php

class m130827_074609_profile_address_field extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{profile}}', 'address', 'TEXT NULL');
	}

	public function down()
	{
	}

}