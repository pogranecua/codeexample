<?php

class m130827_095211_add_subscribe_params_user extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{user}}', 'period_send_mail', 'TINYINT(1) NOT NULL DEFAULT 1');
	}

	public function down()
	{
	}
}