<?php

class m130828_072137_add_column_last_date_send_email extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{profile}}', 'last_date_send_mail', 'TIMESTAMP NULL');
	}

	public function down()
	{
		echo "m130828_072137_add_column_last_date_send_email does not support migration down.\n";
		return false;
	}
}