<?php

class m130905_085920_favoriteGoods extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('{{favoriteGoods}}', array(
            'id'=>'pk',
            'user_id'=>'INT NOT NULL',
            'good_id'=>'INT NOT NULL',
            'date_add'=>'TIMESTAMP NOT NULL',
        ), 'ENGINE=InnoDB');

    }

    public function safeDown()
    {
        $this->dropTable('{{favoriteGoods}}');
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}