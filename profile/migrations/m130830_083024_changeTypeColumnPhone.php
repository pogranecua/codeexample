<?php

class m130830_083024_changeTypeColumnPhone extends CDbMigration
{
	public function up()
	{
        $this->alterColumn('{{profile}}','phone','varchar(128) NULL');
	}

	public function down()
	{
		echo "m130830_083024_changeTypeColumnPhone does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}