<?php

class m130909_080900_addFileForUsers extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('{{user_files}}', array(
            'id'=>'pk',
            'name'=>'VARCHAR(128) NOT NULL',
            'description'=>'VARCHAR(128) NOT NULL',
            'file_name'=>'VARCHAR(128) NOT NULL',
            'author_id'=>'INT NOT NULL',
            'date_add'=>'TIMESTAMP NOT NULL',
        ), 'ENGINE=InnoDB');

        $this->createTable('{{files_access}}', array(
            'id'=>'pk',
            'type'=>'VARCHAR(128) NOT NULL', // user, group
            'type_id'=>'INT NOT NULL', // 'admin' (user role), 1 (ID of User)
            'file_id'=>'INT NOT NULL',
        ), 'ENGINE=InnoDB');
    }

    public function safeDown()
    {
        $this->dropTable('{{user_files}}');
        $this->dropTable('{{files_access}}');
    }
	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}