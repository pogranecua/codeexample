<?php

class m130827_075519_drop_column extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('{{profile}}', 'avatar');
        $this->dropColumn('{{profile}}', 'city_id');
	}

	public function down()
	{
		echo "m130827_075519_drop_column does not support migration down.\n";
		return false;
	}

}