<?php

class m130829_134225_addColumnProfile extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{profile}}', 'phone', 'INT NOT NULL');
        $this->addColumn('{{profile}}', 'country', 'varchar(128) NULL');
        $this->addColumn('{{profile}}', 'region', 'varchar(128) NULL');
        $this->addColumn('{{profile}}', 'city', 'varchar(128) NULL');
	}

	public function down()
	{
		echo "m130829_134225_addColumnProfile does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}