<?php

class m130827_112325_subscribe_categories extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{profile_subscribe_cat}}', array(
            'id'=>'pk',
            'user_id'=>'INT NOT NULL',
            'cat_id'=>'INT NOT NULL'
        ));
	}

	public function down()
	{

	}
}