<?php

class m130903_125349_Country extends CDbMigration
{
    public function safeUp()
    {
        $this->runScript('geo.sql');
    }

	public function down()
	{
		echo "m130903_125349_Country does not support migration down.\n";
		return false;
	}
    protected function runScript($script) {

        $path = Yii::getPathOfAlias('application.modules.profile.migrations.sql');
        $filePath = $path."/".$script;
        $absPath = realpath($filePath);
        $sql = file_get_contents($absPath);

        $sqlArray = explode(';', $sql);
        foreach ($sqlArray as $stmt) {
            if (strlen(trim($stmt)) > 0)
                $this->execute($stmt);
        }
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}