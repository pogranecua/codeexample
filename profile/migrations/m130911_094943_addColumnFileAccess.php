<?php

class m130911_094943_addColumnFileAccess extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{files_access}}', 'status', 'TINYINT(1) NULL');
	}

	public function down()
	{
		echo "m130911_094943_addColumnFileAccess does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}