<?php

class m130607_081428_profile extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('{{profile}}', array(
            'id'=>'pk',
            'user_id'=>'INT NOT NULL',
            'name'=>'VARCHAR(128) NOT NULL',
	        'social_link'=>'VARCHAR(128) NOT NULL',
            'city_id'=>'INT NOT NULL',
	        'sex'=>'TINYINT(1) NULL',
            'date_birth'=>'TIMESTAMP NOT NULL',
            'avatar'=>'VARCHAR(128) NOT NULL',
        ), 'ENGINE=InnoDB');

    }

    public function safeDown()
    {
        $this->dropTable('{{profile}}');
    }
}
