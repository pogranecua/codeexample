<?php

class m130828_070906_remove_user_add_profile_column extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('{{user}}', 'period_send_mail');
        $this->dropColumn('{{user}}', 'status_subscribe');
        $this->addColumn('{{profile}}', 'status_subscribe', 'TINYINT(1) NOT NULL DEFAULT 1');
        $this->addColumn('{{profile}}', 'period_send_mail', 'TINYINT(1) NOT NULL DEFAULT 1');
	}

	public function down()
	{
	}
}